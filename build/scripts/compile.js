const fs = require('fs-extra')
const path = require('path')
const chalk = require('chalk')
const webpack = require('webpack')
const webpackConfig = require('../webpack.config')
const project = require('../../project.config')

const runWebpackCompiler = (webpackConfig) =>
  new Promise((resolve, reject) => {
    webpack(webpackConfig).run((err, stats) => {
      if (err) {
        console.log('Webpack compiler encountered a fatal error.', err)
        return reject(err)
      }

      const jsonStats = stats.toJson()
      if (jsonStats.errors.length > 0) {
        console.log('Webpack compiler encountered errors.')
        console.log(jsonStats.errors.join('\n'))
        return reject(new Error('Webpack compiler encountered errors'))
      } else if (jsonStats.warnings.length > 0) {
        console.log('Webpack compiler encountered warnings.')
        console.log(jsonStats.warnings.join('\n'))
      }
      resolve(stats)
    })
  })

const compile = () => Promise.resolve()
  .then(() => console.log('Starting compiler...'))
  .then(() => console.log('Target application environment: ' + chalk.bold(project.env)))
  .then(() => runWebpackCompiler(webpackConfig))
  .then((stats) => {
    console.log(`Copying static assets from ./public_static to ./${project.outDir}.`)
    fs.copySync(
      path.resolve(project.basePath, 'public_static'),
      path.resolve(project.basePath, project.outDir)
    )
    return stats
  })
  .then((stats) => {
    if (project.verbose) {
      console.log(stats.toString({
        colors: true,
        chunks: false,
      }))
    }
    console.log(`Compiler finished successfully! See ./${project.outDir}.`)
  })
  .catch((err) => console.log('Compiler encountered errors.', err))

compile()
