const NODE_ENV = process.env.NODE_ENV || 'development'

module.exports = {
  env: NODE_ENV,
  basePath: __dirname,
  srcDir: 'src',
  main: 'main',
  outDir: 'public',
  publicPath: '/',
  sourcemaps: true,
  externals: {},
  globals: {},
  verbose: false,
  vendors: [
    'react',
    'react-dom',
    'redux',
    'react-redux',
    'redux-thunk',
    'react-router',
  ],
}
