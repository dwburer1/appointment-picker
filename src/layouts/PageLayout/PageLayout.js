import React from 'react'
import PropTypes from 'prop-types'
import { Header } from 'semantic-ui-react'

import './PageLayout.scss'

export const PageLayout = ({ children }) => (
  <div className='container'>
    <Header as='h3'>Cox Coding Assignment - Appointment Widget</Header>
    <p>Daniel Burer - <a href='mailto:hire@dburer.com'>hire@dburer.com</a></p>
    <p>{'Git repository: '}
      <a href='https://gitlab.com/dwburer1/appointment-picker'>https://gitlab.com/dwburer1/appointment-picker</a>
    </p>
    <div className='content'>
      {children}
    </div>
  </div>
)
PageLayout.propTypes = {
  children: PropTypes.node,
}

export default PageLayout
