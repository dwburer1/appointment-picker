import React from 'react'
import PropTypes from 'prop-types'
import { Button, Card, Header, Icon, Input, List, Message, Modal, Table } from 'semantic-ui-react'

import './Appointments.scss'

class Appointments extends React.Component {
  constructor (props) {
    super(props)
    this.setState({
      phoneEmpty: false,
      nameEmpty: false
    })
  }

  // Color values pulled from Semantic-UI
  positiveStyles = {
    background: '#fcfff5',
    color: '#2c662d'
  }

  negativeStyles = {
    background: '#fff6f6',
    color: '#9f3a38'
  }

  // Default state
  state = {
    open: false
  }

  show = (hour) => {
    const appointments = this.props.appointments

    if (!appointments[hour].available) {
      this.setState({
        name: appointments[hour].name,
        phone: appointments[hour].phone
      })
    }

    this.setState({
      open: true,
      hour: hour
    })
  }

  clear = () => {
    this.setState({
      open: false,
      name: undefined,
      phone: undefined,
      phoneEmpty: false,
      phoneInvalid: false,
      nameEmpty: false
    })
  }

  isMissingValue = (value) => {
    return value === '' || typeof (value) === 'undefined' || value == null
  }

  handleSubmit = () => {
    // Check values exist
    const validName = !this.isMissingValue(this.state.name)
    let validPhone = !this.isMissingValue(this.state.phone)

    this.setState({
      nameEmpty: !validName,
      phoneEmpty: !validPhone
    })

    if (validPhone) {
      // Verify formatting
      validPhone = /\d{10}/.test(this.state.phone)
      this.setState({
        phoneInvalid: !validPhone,
      })
    } else {
      this.setState({
        phoneInvalid: false,
      })
    }

    if (validName && validPhone) {
      this.props.updateAppointments({
        hour: this.state.hour,
        available: false,
        name: this.state.name,
        phone: this.state.phone
      })
      this.clear()
    }
  }

  handleRemove = (hour) => {
    this.props.updateAppointments({
      hour: hour,
      available: true,
      name: undefined,
      email: undefined
    })
    this.forceUpdate()
  }

  handleNameInput = (event) => {
    this.setState({
      name: event.target.value
    })
  }

  handlePhoneInput = (event) => {
    this.setState({
      phone: event.target.value
    })
  }

  render () {
    const { appointments } = this.props
    const { open } = this.state
    return (
      <div>
        <Table celled striped definition>
          <Table.Body>
            {Object.keys(appointments).map((key) => {
              let hourData = appointments[key]
              let available = hourData.available

              return (
                <Table.Row key={key}>
                  <Table.Cell textAlign='left' verticalAlign='middle' collapsing>
                    {key}
                  </Table.Cell>
                  <Table.Cell verticalAlign='middle'>
                    <Card fluid style={available ? this.positiveStyles : this.negativeStyles}>
                      <Card.Content>
                        <div>
                          <Card.Description>
                            {!available &&
                              <Button
                                className='edit-button-inline'
                                basic
                                floated='right'
                                icon='edit'
                                onClick={() => this.show(key)} />
                            }
                            <b>{available ? 'Open' : 'Unavailable'}</b>
                          </Card.Description>
                          <Card.Description>
                            {!available ? hourData.name : 'Available for appointment'}
                          </Card.Description>
                        </div>
                      </Card.Content>
                    </Card>
                  </Table.Cell>
                  <Table.Cell textAlign='right' verticalAlign='middle' collapsing>
                    {hourData.available
                      ? <Button style={{ margin: 0 }} icon='plus' onClick={() => this.show(key)} />
                      : <div>
                        <Button
                          className='edit-button-discrete'
                          basic
                          icon='edit'
                          onClick={() => this.show(key)} />
                        <Button negative style={{ margin: 0 }} icon='x icon' onClick={() => this.handleRemove(key)} />
                      </div>
                    }
                  </Table.Cell>
                </Table.Row>
              )
            })}
          </Table.Body>
        </Table>
        <Modal size='large' open={open} onClose={this.clear} closeIcon>
          <Header content='Update Time Slot' />
          <Modal.Content>
            <p>
              Please add your name and phone number below to create an appointment for: <b>{this.state.hour}</b>
            </p>
            <Input
              style={{ marginBottom: '0.5em' }}
              fluid
              icon='users'
              iconPosition='left'
              size='large'
              placeholder='Name'
              maxLength='32'
              value={this.state.name}
              onChange={this.handleNameInput}
              error={this.state.nameEmpty} />
            <Input
              type='tel'
              fluid
              icon='phone'
              iconPosition='left'
              size='large'
              placeholder='Phone Number (format: xxxxxxxxxx):'
              maxLength='10'
              value={this.state.phone}
              onChange={this.handlePhoneInput}
              error={this.state.phoneEmpty} />
            {(this.state.nameEmpty || this.state.phoneEmpty) &&
              <Message negative>
                <Message.Header>Error</Message.Header>
                <p>Please fill out the following:</p>
                <List bulleted>
                  {this.state.nameEmpty && <List.Item>Name</List.Item>}
                  {this.state.phoneEmpty && <List.Item>Phone number</List.Item>}
                </List>
              </Message>
            }
            {(this.state.phoneInvalid) &&
            <Message negative>
              <Message.Header>Error</Message.Header>
              <p>Phone number is not properly formatted.</p>
            </Message>
            }
          </Modal.Content>
          <Modal.Actions>
            <Button color='red' onClick={this.clear}>
              <Icon name='remove' /> Cancel
            </Button>
            <Button color='green' onClick={this.handleSubmit}>
              <Icon name='checkmark' /> Submit
            </Button>
          </Modal.Actions>
        </Modal>
      </div>
    )
  }
}

Appointments.propTypes = {
  appointments: PropTypes.object.isRequired,
  updateAppointments: PropTypes.func.isRequired
}

export default Appointments
