import { connect } from 'react-redux'
import { updateAppointments } from '../modules/appointments'
import Appointments from '../components/Appointments'

const mapDispatchToProps = {
  updateAppointments
}

const mapStateToProps = (state) => ({
  appointments : state.appointments
})

export default connect(mapStateToProps, mapDispatchToProps)(Appointments)
