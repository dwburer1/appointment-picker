import { injectReducer } from '../../store/reducers'

export default (store) => ({
  path : 'appointments',
  getComponent (nextState, cb) {
    require.ensure([], (require) => {
      const Appointments = require('./containers/AppointmentsContainer').default
      const reducer = require('./modules/appointments').default

      injectReducer(store, { key: 'appointments', reducer })

      cb(null, Appointments)
    }, 'appointments')
  }
})
