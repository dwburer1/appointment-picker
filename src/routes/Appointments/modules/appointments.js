export const APPOINTMENTS_UPDATE = 'APPOINTMENTS_UPDATE'

export function updateAppointments (value) {
  return {
    type: APPOINTMENTS_UPDATE,
    payload: value
  }
}

export const actions = {
  updateAppointments
}

const ACTION_HANDLERS = {
  [APPOINTMENTS_UPDATE]: (state, action) => {
    // Update the appointments data structure
    const hour = action.payload.hour
    state[hour].available = action.payload.available
    state[hour].name = action.payload.name
    state[hour].phone = action.payload.phone
    return state
  }
}

const initialState = {
  '09:00 AM': {
    available: true
  },
  '10:00 AM': {
    available: true
  },
  '11:00 AM': {
    available: false,
    name: 'John Appleseed',
    phone: '5552221234'
  },
  '12:00 PM': {
    available: true
  },
  '01:00 PM': {
    available: true
  },
  '02:00 PM': {
    available: false,
    name: 'Stacy Smith',
    phone: '5553334321'
  },
  '03:00 PM': {
    available: true
  },
  '04:00 PM': {
    available: true
  }
}

export default function appointmentsReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
