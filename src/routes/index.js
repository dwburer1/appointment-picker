import CoreLayout from '../layouts/PageLayout/PageLayout'
import AppointmentsRoute from './Appointments'

export const createRoutes = (store) => ({
  path        : '/',
  component   : CoreLayout,
  indexRoute  :  AppointmentsRoute(store)
})

export default createRoutes
